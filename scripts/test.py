import logging
import time

import web3
import websocket

from gas_proxy.web3 import GasMiddleware

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

re_websocket = r'wss?://'

def socket_constructor():
    return websocket.create_connection('ws://localhost:8545')

GasMiddleware.socket_constructor = socket_constructor
GasMiddleware.last_gas = "0x1000"

w3 = web3.Web3(web3.Web3.WebsocketProvider('ws://localhost:8545'))
w3.middleware_onion.add(GasMiddleware)

while True:
    print(w3.eth.gas_price())
    time.sleep(1)
