import unittest

from gas_proxy.cache.mem import MemCache


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass



    def test_memcache(self):
        memcache = MemCache()
        memcache.set(10)
        self.assertEqual(memcache.get(), 10)
    

if __name__ == '__main__':
    unittest.main()
