import json
import logging

import unittest

from gas_proxy.web3 import GasMiddleware

logging.basicConfig(level=logging.DEBUG)


class Mocket:


    def __init__(self):
        self.last_id = None
        self.response = None
        self.active = True


    def toggle(self):
        self.active = not self.active


    def send(self, d):
        if not self.active:
            raise ConnectionError('socket deactivated')
        o = json.loads(d)
        self.last_id = o['id']
        r = {
            "jsonrpc": "2.0",
            "id": self.last_id,
            "result": "0x1324",
                }
        self.response = json.dumps(r)

    def recv(self, c=0):
        return self.response


class MiddlewareTest(unittest.TestCase):


    def setUp(self):
        self.socket = Mocket()

        def socket_constructor():
            return self.socket

        GasMiddleware.socket_constructor = socket_constructor


    def tearDown(self):
        pass


    def test_middleware(self):
        GasMiddleware.last_gas = "0x2a"
        middleware = GasMiddleware(None, None)

        self.socket.toggle()
        r = middleware.__call__("eth_gasPrice", [])
        self.assertEqual(r['result'], "0x2a")

        self.socket.toggle()
        r = middleware.__call__("eth_gasPrice", [])
        self.assertEqual(r['result'], "0x1324")

        self.socket.toggle()
        r = middleware.__call__("eth_gasPrice", [])
        self.assertEqual(r['result'], "0x1324")

if __name__ == '__main__':
    unittest.main()
