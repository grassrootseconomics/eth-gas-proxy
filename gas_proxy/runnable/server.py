import re
import os
import argparse
import logging
import socket
import json

import websocket

from gas_proxy.proxy import JSONRPCOracle as Oracle
from gas_proxy.cache.mem import MemCache

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()


argparser = argparse.ArgumentParser()
#argparser.add_argument('-c', type=str, default=config_dir, help='config file')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('--host', default='localhost', type=str)
argparser.add_argument('--port', default=8545, type=int)
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be more verbose')
argparser.add_argument('provider', type=str, help='Gas price provider url')
args = argparser.parse_args()

if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)

re_websocket = r'^wss?://(.+):(\d+)/?$'
m = re.match(re_websocket, args.provider)
if m == None:
    raise ValueError('websocket providers only')

logg.debug('using websocket host {} port {}'.format(m.group(1), m.group(2)))
connection_string = args.provider


def main():
    memcache = MemCache()

    ws = websocket.create_connection(connection_string)
    ws = Oracle(ws, cache=memcache)

    s = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind((args.host, args.port))
    s.listen(10)

    while True:
        (csock, caddr) = s.accept()
        d = csock.recv(4096)
        o = json.loads(d)
        response = {
            "jsonrpc": "2.0",
            "id": o['id'],
            "result": ws.get(),
                }
        csock.send(json.dumps(response).encode('utf-8'))


if __name__ == '__main__':
    main()
