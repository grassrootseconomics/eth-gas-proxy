from .base import Cache

class MemCache(Cache):


    def __init__(self):
        self.lastvalue = 0


    def get(self):
        return self.lastvalue


    def set(self, value):
        self.lastvalue = value
