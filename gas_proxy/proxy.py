import json
import logging

import websocket

logg = logging.getLogger()


class Oracle:

    def get(self):
        raise NotImplementedError

class JSONRPCOracle(Oracle):


    def __init__(self, socket, cache=None):
        self.query = {
            "jsonrpc": "2.0",
            "method": "eth_gasPrice",
            "params": [],
            "id": 0,
        }
        self.id_seq = 0
        self.socket = socket
        self.cache = cache


    def __del__(self):
        self.socket.close()


    def get(self, request_id=None):
        o = None
        if request_id == None:
            request_id = str(self.id_seq)
            self.id_seq += 1
   
        self.query['id'] = request_id

        try:
            self.socket.send(json.dumps(self.query))
            result = self.socket.recv()
            o = json.loads(result)
            result_hex = o['result'][2:]
            if len(result_hex) % 2 != 0:
                result_hex = '0' + result_hex
            result_bytes = bytes.fromhex(result_hex)
            result_num = int.from_bytes(result_bytes, 'big')
        except Exception as e:
            logg.exception('backend error')
            if self.cache == None:
                raise(e)
            result_num = self.cache.get()

        if self.cache != None:
            self.cache.set(result_num)
        return result_num
